<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Relation;
use App\Models\Category;

class Note extends Model
{
    use HasFactory;

    public function tags() {
        $ids = Relation::where('note_id', $this->id)->select('category_id')->get()->toArray();
        $tags = Category::whereIn('id', $ids)->get();
        return $tags;
    }

    static function getByTags($query) {
        if($query != null) {
            $tags = Category::where('name', 'LIKE', '%' . $query . '%')->select('id')->get()->toArray();
            $ids = Relation::whereIn('category_id', $tags)->select('note_id')->get()->toArray();
            $notes = Note::whereIn('id', $ids)->get();
            return $notes;
        }
        else {
            return Note::all();
        }
    }
}
